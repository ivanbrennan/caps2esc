# tab2meta

## Dependencies

- [Interception Tools][interception-tools]

## Building

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```

## Execution

`tab2meta` is an [_Interception Tools_][interception-tools] plugin. A suggested
`udevmon` job configuration is:

```yaml
- JOB: "intercept -g $DEVNODE | tab2meta 0.1 | uinput -d $DEVNODE"
  DEVICE:
    EVENTS:
      EV_KEY: [KEY_TAB]

```

For more information about the [_Interception Tools_][interception-tools], check
the project's website.
